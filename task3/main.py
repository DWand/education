#!/usr/bin/python

""" Golden Pyramid """

from copy import deepcopy


def _get_pyramid():
    """
    Retrieves pyramid from the user

    Return:
        list: a list of levels, which are lists of values
    """
    pyramid = []
    levels = int(raw_input('How many levels does the pyramid have? '))

    for level in range(0, levels):
        print 'Input level {}'.format(level + 1)
        pyramid.append([])

        for position in range(0, level + 1):
            number = float(raw_input('p[{}][{}] = '.format(level, position)))
            pyramid[level].append(number)

    return pyramid


def _get_max_path_value(pyramid):
    """
    Finds a maximal sum of all elements in a path from the top element of the
    pyramid to its bottom level

    Args:
        pyramid (list): A list of lists of floats which represent a pyramid

    Return:
        float: A sum of elements on the path
    """
    sums = deepcopy(pyramid)
    penult_level = len(pyramid) - 2
    for level in range(penult_level, -1, -1):
        for position in range(0, len(sums[level])):
            left_sum = sums[level][position] + sums[level + 1][position]
            right_sum = sums[level][position] + sums[level + 1][position + 1]
            sums[level][position] = max(left_sum, right_sum)
    return sums[0][0]


def main():
    """ Main entry point of the program """
    pyramid = _get_pyramid()
    max_path = _get_max_path_value(pyramid)
    print 'Max path has {} value.'.format(max_path)


if __name__ == '__main__':
    main()
