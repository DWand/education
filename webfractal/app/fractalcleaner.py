from threading import Timer


class FractalCleaner(object):
    """
    A cleaner which allows to delete old fractals from the folder
    """

    def __init__(self, config, helper):
        """
        Initializes the fractal cleaner

        :param dict config: a configuration dictionary
        :param FractalHelper helper: a helper to deal with fractals
        """
        self.helper = helper
        self.timer = None

        conf = config.get('cleaner', {})
        self.interval = conf.get('interval', 3600 * 24)
        self.max_age = conf.get('max_age', 3600 * 24 * 3)

    def _start_timer(self):
        """
        Starts a next timer
        """
        self.timer = Timer(self.interval, self._on_interval)
        self.timer.start()

    def _on_interval(self):
        """
        An interval passed event handler
        """
        print 'Cleaner: deleting old fractals'
        self.helper.delete_old_fractals(self.max_age)
        self._start_timer()

    def start(self):
        """
        Starts a cleaner
        """
        if self.timer is None:
            self._start_timer()

    def stop(self):
        """
        Stops a cleaner
        """
        if self.timer is not None:
            self.timer.cancel()
            self.timer = None
