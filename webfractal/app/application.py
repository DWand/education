import logging

from werkzeug.routing import Rule
from werkzeug.utils import redirect
from werkzeug.exceptions import NotFound
from redis import Redis

from baseapplication import BaseApplication
from fractalhelper import FractalHelper
from fractalcleaner import FractalCleaner
from utils import Pagination


class Application(BaseApplication):
    """
    A flame fractal WSGI application
    """

    def __init__(self, config):
        super(Application, self).__init__(config)
        self.logger = logging.getLogger(__name__)

        redis_config = config.get('redis', {})
        self.redis = Redis(
            host=redis_config.get('host', 'localhost'),
            port=redis_config.get('port', 6379)
        )

        self.helper = FractalHelper(config, self.redis)
        self._init_cleaner()

    def _init_cleaner(self):
        if 'cleaner' in self.config:
            self.cleaner = FractalCleaner(self.config, self.helper)
            self.cleaner.start()
        else:
            self.cleaner = None

    def _get_routes(self):
        """
        Defines routes of the application
        :return: a list of application's routes
        :rtype: list of werkzeug.routing.Rule
        """
        return [
            Rule('/', endpoint='on_new_fractal'),
            Rule('/show/<name>', endpoint='on_show_fractal')
        ]

    def on_new_fractal(self, request):
        """
        An endpoint for creating new fractals

        :param werkzeug.wrappers.Request request: a request context
        """
        fractals_list = self._get_fractals_list(request)

        error = None
        if request.method == 'POST':
            title = request.form.get('title', 'Unknown')
            iters = self._parse_int(request.form.get('iters'), 0)
            points = self._parse_int(request.form.get('points'), 0)
            seed = self._parse_int(request.form.get('seed'), 0)
            transforms = self._parse_int(request.form.get('transforms'), 0)
            gamma = self._parse_float(request.form.get('gamma'), 0)

            try:
                fractal_name = self.helper.build_fractal(
                    title, points, iters, transforms, seed, gamma
                )
                return redirect('/show/{}'.format(fractal_name))
            except (OSError, IOError) as e:
                self.logger.error('Unable to save %s.', e.filename)
                error = 'Unable to save a fractal.'

        return self._render_template(
            'new.html',
            error=error,
            fractals=fractals_list
        )

    def on_show_fractal(self, request, name):
        """
        An endpoint for displaying fractals

        :param werkzeug.wrappers.Request request: a request context
        :param str name: a name of the fractal to display
        """
        fractals_list = self._get_fractals_list(request)

        if self.helper.is_fractal(name):
            info = self.helper.get_fractal(name)
            return self._render_template(
                'show.html',
                fractal=info,
                fractals=fractals_list
            )
        else:
            raise NotFound()

    def _get_fractals_list(self, request):
        """
        Fetches a list of previously generated fractals

        :param werkzeug.wrappers.Request request: a request context
        :return: a tuple which consists of a list of fractal names and
            related pagination object
        :rtype: (list of str, utils.Pagination)
        """
        page = self._parse_int(request.args.get('page', 1), 1)
        limit = self.config.get('page_size', 10)
        total = self.helper.count_fractals()

        fractals = self.helper.get_fractals_list((page - 1) * limit, limit)
        pagination = Pagination(page, limit, total)

        return fractals, pagination

    def _handle_not_found_error(self, error, request):
        """
        Handles Not Found error

        :param werkzeug.exceptions.NotFound error: an error object
        :param werkzeug.wrappers.Request request: a request to handle
        :return: a response object
        :rtype: Response
        """
        fractals_list = self._get_fractals_list(request)
        return self._render_template('error404.html', fractals=fractals_list)
