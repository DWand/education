import logging.config

from werkzeug.wsgi import SharedDataMiddleware
from werkzeug.routing import Map
from werkzeug.wrappers import Request, Response
from werkzeug.exceptions import HTTPException, NotFound
from jinja2 import Environment, FileSystemLoader


class BaseApplication(object):
    """
    A base class for WSGI application
    """

    def __init__(self, config):
        """ Initializes an application """
        self.config = config

        if 'logging' in config:
            logging.config.dictConfig(config.get('logging', {}))
        self.logger = logging.getLogger(__name__)

        if 'static_folders' in config:
            self.process_request = SharedDataMiddleware(
                self.process_request,
                config.get('static_folders')
            )

        self._url_map = Map(self._get_routes())

        self._jinja = Environment(
            loader=FileSystemLoader(config.get('tpl_path')),
            autoescape=config.get('tpl_autoescape', True)
        )

    def _get_routes(self):
        """
        Defines routes of the application
        :return: a list of routes
        :rtype: list of werkzeug.routing.Route
        """
        return []

    def __call__(self, environ, start_response):
        """
        WSGI request handling entry point

        :param dict environ: the WSGI environment
        :param callable start_response: the response callable
            provided by the WSGI server.
        :return: an application iterator
        """
        return self.process_request(environ, start_response)

    def process_request(self, environ, start_response):
        """
        Initializes a request and handles it

        :param dict environ: the WSGI environment
        :param callable start_response: the response callable
            provided by the WSGI server.
        :return: an application iterator
        """
        request = Request(environ)
        response = self.__get_response(request)
        return response(environ, start_response)

    def __get_response(self, request):
        """
        Determines and executes a request handler

        :param Request request: a request to handle
        :return: a response for the request
        :rtype: Response
        """
        adapter = self._url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            return getattr(self, endpoint)(request, **values)

        except NotFound as e404:
            self.logger.warning('Page is not found: %s.', request.url)
            return self._handle_not_found_error(e404, request)

        except HTTPException as e:
            self.logger.error('HTTPException: %s.', e.description)
            return e

    def _handle_not_found_error(self, error, request):
        """
        Handles Not Found error

        :param werkzeug.exceptions.NotFound error: an error object
        :param Request request: a request to handle
        :return: a response object
        :rtype: Response
        """
        return error

    def _render_template(self, name, **context):
        """ Renders a specified template """
        tpl = self._jinja.get_template(name)
        return Response(tpl.render(context), mimetype='text/html')

    @classmethod
    def _parse_int(cls, value, default):
        """
        Parses value as int. If it is impossible to interpret the value as
        an integer number, the default value will be used.

        :param str value: a value to parse
        :param int default: a default value to use in case of errors
        :return: parsed value
        :rtype: int
        """
        result = default
        try:
            result = int(value)
        except ValueError:
            pass
        return result

    @classmethod
    def _parse_float(cls, value, default):
        """
        Parses value as float. If it is impossible to interpret the value as
        a float number, the default value will be used.

        :param str value: a value to parse
        :param float default: a default value to use in case of errors
        :return: parsed value
        :rtype: float
        """
        result = default
        try:
            result = float(value)
        except ValueError:
            pass
        return result
