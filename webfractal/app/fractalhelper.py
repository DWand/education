import uuid
import os
import logging
from time import time

from fractal import FractalBuilder


class FractalHelper(object):
    """
    A helper class for manipulations with fractals
    """

    FILE_EXT = 'png'
    TBL_FRACTALS = 'fractals'
    TBL_FRACTAL_TPL = 'fractal:{}'

    def __init__(self, config, redis):
        """
        :param dict config: an application configuration object
        :param redis.Redis redis: a DB connection object
        """
        self.config = config
        self.redis = redis
        self.logger = logging.getLogger(__name__)

        fractal_config = self.config.get('fractal')
        self.img_base_path = fractal_config.get('img_path')
        self.img_width = fractal_config.get('img_width')
        self.img_height = fractal_config.get('img_height')

    def build_fractal(self, title, points, iters, transforms, seed, gamma):
        """
        Builds and saves a new flame fractal

        :param str title: a title of the fractal
        :param int points: an amount of starting points
        :param int iters: an amount of iterations to perform
        :param int transforms: an amount of transformations to use
        :param int seed: a seed to use in random generator
        :param float gamma: a value to use for gamma correction
        :return: a name of the saved fractal
        :rtype: str
        """
        builder = FractalBuilder(
            self.img_width,
            self.img_height,
            points=points,
            iterations=iters,
            transformations=transforms,
            seed=seed,
            gamma=gamma
        )
        builder.build()

        image = builder.get_image()
        name = uuid.uuid1().hex
        path = self.get_fractal_path(name)

        image.save(path, self.FILE_EXT)
        self.redis.lpush(self.TBL_FRACTALS, name)
        self.redis.hmset(self.TBL_FRACTAL_TPL.format(name), {
            'name': name,
            'title': title,
            'time': time(),
            'width': builder.width,
            'height': builder.height,
            'points': points,
            'iterations': iters,
            'transformations': transforms,
            'seed': seed,
            'gamma': gamma
        })
        self.redis.save()

        return name

    def get_fractal(self, name):
        """
        Fetches information about the fractal from database
        :param str name: a name of the fractal
        :return: an information about the fractal
        :rtype: dict or None
        """
        key = self.TBL_FRACTAL_TPL.format(name)
        fractal = self.redis.hgetall(key)

        if not fractal:
            return None

        fractal['path'] = self.get_fractal_path(name)
        fractal['url'] = self.get_fractal_url(name)
        return fractal

    def get_fractal_path(self, name):
        """
        Constructs an absolute path for a given fractal image

        :param str name: a name of the target fractal
        :return: an absolute path of the fractal image
        :rtype: str
        """
        filename = '{}.{}'.format(name, self.FILE_EXT)
        path = os.path.join(self.img_base_path, filename)
        return path

    def get_fractal_url(self, name):
        """
        Constructs a url for a given fractal image

        :param str name: a name of the target fractal
        :return: a url of the fractal image
        :rtype: str
        """
        path = self.get_fractal_path(name)
        return os.path.relpath(path, self.config.get('base_path'))

    def is_fractal(self, name):
        """
        Checks if a fractal with the given name exists in the folder

        :param str name: a name of the target fractal
        :return: True if fractal exists, False otherwise
        :rtype: bool
        """
        key = self.TBL_FRACTAL_TPL.format(name)
        exists = self.redis.exists(key)
        return exists

    def count_fractals(self):
        """
        Counts fractals in the folder

        :return: an amount of fractals in the folder
        :rtype: int
        """
        length = self.redis.llen(self.TBL_FRACTALS)
        return length

    def get_fractals_list(self, offset, limit):
        """
        Fetches a list of fractals from the folder

        :param int offset: a starting position to start fetching from
        :param int limit: an amount of fractals to fetch
        :return: a list of fractal names
        :rtype: list of str
        """
        end = offset + limit - 1
        names = self.redis.lrange(self.TBL_FRACTALS, offset, end)
        return names

    def delete_old_fractals(self, max_age):
        """
        Deletes old fractals from the folder

        :param int max_age: a maximum possible age of file
        """
        now = time()
        key_tpl = self.TBL_FRACTAL_TPL

        keys = self.redis.lrange(self.TBL_FRACTALS, 0, -1)

        pipe = self.redis.pipeline()
        for key in keys:
            pipe.hgetall(key_tpl.format(key))
        fractals = pipe.execute()

        fractals = filter(lambda f: float(f['time']) + max_age < now, fractals)

        pipe = self.redis.pipeline()
        for fractal in fractals:
            try:
                name = fractal['name']
                pipe.lrem(self.TBL_FRACTALS, name)
                os.unlink(self.get_fractal_path(name))
            except OSError as e:
                self.logger.error('Unable to delete %s.', e.filename)
        pipe.execute()

        keys = map(lambda f: key_tpl.format(f['name']), fractals)
        self.redis.delete(keys)
        self.redis.save()
