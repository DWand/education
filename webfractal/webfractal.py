"""
A web application for flame fractal generation
"""

import os

from werkzeug.serving import run_simple, is_running_from_reloader

from app.application import Application


def create_app():
    """
    Creates a new application
    :rtype: Application
    """
    basedir = os.path.dirname(__file__)
    config = {
        'base_path': basedir,
        'static_folders': {
            '/css': os.path.join(basedir, 'css'),
            '/img': os.path.join(basedir, 'img'),
        },
        'tpl_path': os.path.join(basedir, 'app', 'templates'),
        'fractal': {
            'img_path': os.path.join(basedir, 'img'),
            'img_width': 500,
            'img_height': 250,
        },
        'logging': {
            'version': 1,
            'formatters': {
                'verbose': {
                    'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
                },
                'simple': {
                    'format': '%(levelname)s %(message)s'
                },
            },
            'handlers': {
                'console': {
                    'class': 'logging.StreamHandler',
                    'formatter': 'simple',
                    'level': 'INFO'
                },
                'file': {
                    'class': 'logging.handlers.TimedRotatingFileHandler',
                    'formatter': 'verbose',
                    'filename': os.path.join(basedir, 'log.log'),
                    'when': 'D',
                    'backupCount': 3,
                    'level': 'INFO'
                },
                'error_file': {
                    'class': 'logging.handlers.TimedRotatingFileHandler',
                    'formatter': 'verbose',
                    'filename': os.path.join(basedir, 'error.log'),
                    'when': 'D',
                    'backupCount': 3,
                    'level': 'ERROR'
                }
            },
            'root': {
                'handlers': ['console', 'file', 'error_file'],
                'level': 'INFO'
            }
        },
        'redis': {
            'host': 'localhost',
            'port': 6379
        },
        'page_size': 10
    }
    if not is_running_from_reloader():
        config['cleaner'] = {
            'interval': 60 * 15,
            'max_age': 60 * 30
        }
    application = Application(config)
    return application


if __name__ == '__main__':
    app = create_app()
    run_simple('127.0.0.1', 5000, app, use_debugger=True, use_reloader=True)
