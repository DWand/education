class ExitOnDemand(Exception):
    """ An exception to point that the user wants to close an application """
    pass
