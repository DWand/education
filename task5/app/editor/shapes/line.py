from app.editor.shapes.abstractshape import AbstractShape


class Line(AbstractShape):
    """ A line primitive """

    def __init__(self, color, x1, y1, x2, y2):
        """
        Initializes a line.

        :param color: a color of the line
        :type color: tuple or str
        :param int x1: an X coordinate of the start point of the line
        :param int y1: an Y coordinate of the start point of the line
        :param int x2: an X coordinate of the end point of the line
        :param int y2: an Y coordinate of the end point of the line
        """
        super(Line, self).__init__(color)
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def __str__(self):
        return 'Line: ({}, {}) -> ({}, {}), color: {}'.format(
            self.x1, self.y1, self.x2, self.y2, self.color
        )

    def draw(self, graphics):
        graphics.draw_line(self.x1, self.y1, self.x2, self.y2, self.color)
