from app.editor.shapes.abstractshape import AbstractShape


class Dot(AbstractShape):
    """ A dot primitive """

    def __init__(self, color, x, y):
        """
        Initializes a dot.

        :param color: a color of the dot
        :type color: tuple of str
        :param int x: an X coordinate of the dot
        :param int y: an Y coordinate of the dot
        """
        super(Dot, self).__init__(color)
        self.x = x
        self.y = y

    def __str__(self):
        return 'Dot: ({}, {}), color: {}'.format(self.x, self.y, self.color)

    def draw(self, graphics):
        graphics.draw_point(self.x, self.y, self.color)
