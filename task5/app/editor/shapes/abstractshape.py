class AbstractShape(object):
    """ An abstract graphical primitive """

    def __init__(self, color):
        """
        :param color: a color of the shape
        :type color: tuple or str
        """
        self.color = color

    def draw(self, graphics):
        """
        Draws a shape with a specified graphics object.

        :param graphics.Graphics graphics: a graphics object to use for
            drawing the shape
        """
        raise NotImplementedError('The "draw" method is not implemented')
