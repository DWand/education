from app.editor.shapes.abstractshape import AbstractShape


class Ellipse(AbstractShape):
    """ An ellipse primitive """

    def __init__(self, color, x1, y1, x2, y2):
        """
        Initializes an ellipse.

        :param color: a color of the ellipse
        :type color: tuple or str
        :param int x1: an X coordinate of the top left corner of
            the enclosing rectangle
        :param int y1: an Y coordinate of the top left corner of
            the enclosing rectangle
        :param int x2: an X coordinate of the bottom right corner of
            the enclosing rectangle
        :param int y2: an Y coordinate of the bottom right corner of
            the enclosing rectangle
        """
        super(Ellipse, self).__init__(color)
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def __str__(self):
        return 'Ellipse: ({}, {}) -> ({}, {}), color: {}'.format(
            self.x1, self.y1, self.x2, self.y2, self.color
        )

    def draw(self, graphics):
        graphics.draw_ellipse(self.x1, self.y1, self.x2, self.y2, self.color)
