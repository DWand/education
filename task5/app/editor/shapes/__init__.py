from app.editor.shapes.abstractshape import AbstractShape
from app.editor.shapes.dot import Dot
from app.editor.shapes.line import Line
from app.editor.shapes.rectangle import Rectangle
from app.editor.shapes.ellipse import Ellipse
from app.editor.shapes.polygon import Polygon
