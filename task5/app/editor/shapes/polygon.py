from app.editor.shapes.abstractshape import AbstractShape


class Polygon(AbstractShape):
    """ A polygon primitive """

    def __init__(self, color, points):
        """
        :param color: a color of the polygon
        :type color: tuple or str
        :param points: a list of points the polygon consists of
        :type points: a list of (int,int) tuples
        """
        super(Polygon, self).__init__(color)
        self.points = points

    def __str__(self):
        coords = []
        for point in self.points:
            coords.append('({}, {})'.format(point[0], point[1]))
        return 'Polygon: {}, color: {}'.format(' -> '.join(coords), self.color)

    def draw(self, graphics):
        graphics.draw_polygon(self.points, self.color)
