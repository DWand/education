import os
import jsonpickle

from app.editor.shapes import AbstractShape


class Document(object):
    """ An image document """

    def __init__(self, name, width, height):
        """
        Initializes an image.

        :param str name: a name of the image
        :param int width: a width of the image
        :param int height: a height of the image
        """
        self.name = name
        self.width = width
        self.height = height
        self.shapes = []

    def add_shape(self, shape, index=-1):
        """
        Adds a new shape to the image at the specified index.

        :param shapes.AbstractShape shape: a shape to add to the image
        :param int index: an index which points to the position to insert
            a new shape to. Defaults to -1. If the index equals -1, the shape
            will be appended to the end of the list of shapes.
        """
        if isinstance(shape, AbstractShape):
            if index == -1:
                self.shapes.append(shape)
            else:
                self.shapes.insert(index, shape)

    def del_shape(self, index):
        """
        Deletes a shape at the specified index.

        :param int index: an index to remove shape at
        """
        try:
            self.shapes.pop(index)
        except IndexError:
            pass

    def draw(self, graphics):
        """
        Draws the image with the specified graphics object

        :param graphics.Graphics graphics: a graphics object to to use
            for drawing the image
        """
        for shape in self.shapes:
            shape.draw(graphics)

    def save(self, force=False):
        """
        Saves the image to a file. A name of the file equals to the name of
            the image.

        :param bool force: whether an existent file has to be overwritten

        :raise NameError: in case a file already exists and force parameter
            is set to False
        """
        if force is False and os.path.isfile(self.name):
            raise NameError()

        with open(self.name, 'w') as f:
            data = jsonpickle.encode(self)
            f.write(data)

    @staticmethod
    def load(filepath):
        """
        Loads an image from a specified file.

        :param str filepath: a path to a file to load an image from
        """
        with open(filepath, 'r') as f:
            data = f.read()
            return jsonpickle.decode(data)
