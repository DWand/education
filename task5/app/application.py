from app.state import State
from app.commands import *
from app.exitondemand import ExitOnDemand


class Application(object):
    """ A console image editor application """

    COMMANDS = {
        'help': HelpCommand,
        'new': NewCommand,
        'save': SaveCommand,
        'load': LoadCommand,
        'show': ShowCommand,
        'draw': DrawCommand,
        'add shape': AddShapeCommand,
        'del shape': DelShapeCommand,
        'change shape': ChangeShapeCommand,
        'exit': ExitCommand,
    }

    def __init__(self):
        """ Initializes an application """

        #: A current state of the application
        self.state = State(None)

    def run(self):
        """ Runs a main loops of the application. """

        print 'Welcome to the simple console image editor.'
        print 'Please, type "help" to get a list of all available commands.'

        try:
            while True:
                command = raw_input('> ')

                if command in self.COMMANDS.keys():
                    self.COMMANDS[command].run(self.state)
                else:
                    print 'Command "{}" is unknown. ' \
                          'Please, type "help" to get a list of all ' \
                          'available commands.'.format(command)

        except (ExitOnDemand, SystemExit, KeyboardInterrupt):
            pass
