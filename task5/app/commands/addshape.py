from app.commands.baseshapecommand import BaseShapeCommand


class AddShapeCommand(BaseShapeCommand):
    """
    A CLI command which allows the user to add a new shape to the image
    """

    @classmethod
    def run(cls, state):
        if state.document is None:
            print 'There are no documents opened.'
        else:
            print 'What kind of shape do you want to add?'
            cls._insert_shape(state)
