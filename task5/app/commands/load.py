from app.commands.basecommand import BaseCommand
from app.editor import Document


class LoadCommand(BaseCommand):
    """
    A CLI command which allows the user to load an image from the file
    """

    @classmethod
    def run(cls, state):
        print 'Enter a path to the file:'
        path = cls._get_string('path')

        try:
            state.document = Document.load(path)
            print 'The document is successfully loaded!'
        except IOError as err:
            print 'Error! Unable to load the document.'
            print '{}: {}'.format(err.strerror, err.filename)
