from app.commands.basecommand import BaseCommand
from app.graphics import Graphics


class DrawCommand(BaseCommand):
    """
    A CLI command which allows the user to draw the image in a window
    """

    @classmethod
    def run(cls, state):
        if state.document is None:
            print 'There are no documents opened.'
        else:
            graphics = Graphics(state.document.width, state.document.height)
            state.document.draw(graphics)
            graphics.invalidate()
