from app.commands.baseshapecommand import BaseShapeCommand


class ChangeShapeCommand(BaseShapeCommand):
    """
    A CLI command which allows the user to change a shape in the image.
    """

    @classmethod
    def run(cls, state):
        if state.document is None:
            print 'There are no documents opened.'
        else:
            print 'Enter an index of the item to change or -1 to cancel.'
            index = cls._get_number('index')
            if index != -1:
                state.document.del_shape(index)

                print 'What kind of shape do you want to insert instead?'
                cls._insert_shape(state, index)
