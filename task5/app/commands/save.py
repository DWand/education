from app.commands.basecommand import BaseCommand

class SaveCommand(BaseCommand):
    """
    A CLI command which allows the user to save the current image to a file
    """

    @classmethod
    def run(cls, state):
        if state.document is None:
            print 'There are no documents opened. Nothing to save.'
            return

        force_overwrite = False
        orig_filename = state.document.name

        while True:
            try:
                state.document.save(force_overwrite)
                print 'The document is successfully saved!'
                break

            except IOError as err:
                state.document.name = orig_filename
                print 'Error! Unable to save the document.'
                print '{}: {}'.format(err.strerror, err.filename)
                break

            except NameError:
                print 'File "{}" already exists! Do you want to overwrite ' \
                      'it?'.format(state.document.name)
                answer = cls._get_string('y/n').lower()

                if answer[0] == 'y':
                    force_overwrite = True
                else:
                    print 'Do you want to rename the image?'
                    answer = cls._get_string('y/n').lower()

                    if answer[0] == 'y':
                        cls._rename_image(state)
                    else:
                        state.document.name = orig_filename
                        print 'File was not saved!'
                        break

    @classmethod
    def _rename_image(cls, state):
        print 'Enter a new path for the image:'
        path = cls._get_string('path')
        state.document.name = path
