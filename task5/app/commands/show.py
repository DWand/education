from app.commands.basecommand import BaseCommand

class ShowCommand(BaseCommand):
    """
    A CLI command which allows the user to show contents of the current image
    """

    @classmethod
    def run(cls, state):
        if state.document is None:
            print 'There are no documents opened.'
        else:
            print 'Current document is "{}" ({}x{})'.format(
                state.document.name,
                state.document.width,
                state.document.height
            )
            print 'Shapes in the document:'
            for index, shape in enumerate(state.document.shapes):
                print '{} -> {}'.format(index, shape)
