import re

from app.commands.basecommand import BaseCommand
from app.editor import *


class BaseShapeCommand(BaseCommand):
    """ A base class for all CLI commands which work with shapes """

    SHAPES = ['dot', 'line', 'rectangle', 'ellipse', 'polygon']
    COLOR_TPL = '^#(?:[0-9a-fA-F]{3}){1,2}$'

    @classmethod
    def _get_color(cls):
        """
        Prompts the user to enter a color.

        :return: a color entered by the user
        :rtype: str
        """
        color = ''
        while True:
            color = cls._get_string('color in hex')
            match = re.search(cls.COLOR_TPL, color)
            if match:
                break
            else:
                print 'Unknown color "{}". Please, enter a color in ' \
                      '#ffffff format.'.format(color)

        return color

    @classmethod
    def _get_rectangular_params(cls):
        """
        Prompts the user to enter parameters of a rectangle.

        :return: a rectangle parameters entered by the user
        :rtype: a (int, int, int, int, str) tuple
        """
        x1 = cls._get_number('x1')
        y1 = cls._get_number('y1')
        x2 = cls._get_number('x2')
        y2 = cls._get_number('y2')
        color = cls._get_color()
        return color, x1, y1, x2, y2

    @classmethod
    def _get_shape_dot(cls):
        """
        Prompts the user to enter parameters of a dot and creates it.

        :return: a dot entered by the user
        :rtype: editor.shapes.Dot
        """
        print 'Enter parameters of the dot:'
        x = cls._get_number('x')
        y = cls._get_number('y')
        color = cls._get_color()
        return Dot(color, x, y)

    @classmethod
    def _get_shape_line(cls):
        """
        Prompts the user to enter parameters of a line and creates it.

        :return: a line entered by the user
        :rtype: editor.shapes.Line
        """
        print 'Enter parameters of the line:'
        color, x1, y1, x2, y2 = cls._get_rectangular_params()
        return Line(color, x1, y1, x2, y2)

    @classmethod
    def _get_shape_rectangle(cls):
        """
        Prompts the user to enter parameters of a rectangle and creates it.

        :return: a rectangle entered by the user
        :rtype: editor.shapes.Rectangle
        """
        print 'Enter parameters of the rectangle:'
        color, x1, y1, x2, y2 = cls._get_rectangular_params()
        return Rectangle(color, x1, y1, x2, y2)

    @classmethod
    def _get_shape_ellipse(cls):
        """
        Prompts the user to enter parameters of an ellipse and creates it.

        :return: an ellipse entered by the user
        :rtype: editor.shapes.Ellipse
        """
        print 'Enter parameters of the ellipse:'
        color, x1, y1, x2, y2 = cls._get_rectangular_params()
        return Ellipse(color, x1, y1, x2, y2)

    @classmethod
    def _get_shape_polygon(cls):
        """
        Prompts the user to enter parameters of a polygon and creates it.

        :return: a polygon entered by the user
        :rtype: editor.shapes.Polygon
        """
        print 'Enter parameters of the polygon:'
        points_count = cls._get_number('Amount of points')

        points = []
        for i in xrange(points_count):
            x = cls._get_number('x{}'.format(i + 1))
            y = cls._get_number('y{}'.format(i + 1))
            points.append((x, y))

        color = cls._get_color()
        return Polygon(color, points)

    @classmethod
    def _insert_shape(cls, state, index=-1):
        """
        Creates and inserts a shape into a specified place in the document

        :param state.State state: a current state of the document
        :param int index: an index to insert a new shape at
        """
        print 'Available shapes are: {}'.format(', '.join(cls.SHAPES))

        kind = None
        while True:
            try:
                kind = cls._get_string('shape').lower()
                if kind in cls.SHAPES:
                    break
                else:
                    raise ValueError()
            except ValueError:
                print 'Unknown kind of shape. Please, try again.'

        method = '_get_shape_' + kind
        shape = getattr(cls, method)()
        state.document.add_shape(shape, index)
