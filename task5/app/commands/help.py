from app.commands.basecommand import BaseCommand


class HelpCommand(BaseCommand):
    """
    A CLI command which allows the user to see a list of available commands
    in the editor
    """

    @classmethod
    def run(cls, state):
        print 'Available commands:'
        print 'help\t\t\tPrint this help message'
        print 'new\t\t\t\tCreate a new document'
        print 'save\t\t\tSave the document'
        print 'load\t\t\tLoad a previously saved document'
        print 'show\t\t\tShow the document'
        print 'draw\t\t\tDraw the document'
        print 'add shape\t\tAdd a new shape to the document'
        print 'del shape\t\tDelete a shape from the document'
        print 'change shape\tChange a shape in the document'
        print 'exit\t\t\tExit the program'
