from app.commands.basecommand import BaseCommand
from app.exitondemand import ExitOnDemand


class ExitCommand(BaseCommand):
    """
    A CLI command which allows the user to exit the editor
    """

    @classmethod
    def run(cls, state):
        raise ExitOnDemand()
