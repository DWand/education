class BaseCommand(object):
    """ A base class for all CLI commands of the editor """

    @staticmethod
    def _get_number(name):
        """
        Prompts the user to enter an integer number.

        :param str name: a name of a parameter to enter

        :return: a number entered by the user
        :rtype: int
        """
        num = 0
        string = ''
        while True:
            try:
                string = raw_input('{}: '.format(name))
                num = int(string)
                break
            except ValueError:
                print 'Unknown number "{}". Please, enter an integer ' \
                      'number.'.format(string)

        return num

    @staticmethod
    def _get_string(name):
        """
        Prompts the user to enter a string.

        :param str name: a name of a parameter to enter

        :return: a string entered by the user
        :rtype: str
        """
        string = raw_input('{}: '.format(name))
        return string

    @classmethod
    def run(cls, state):
        """
        Runs a CLI command.

        :param state.State state: a current state of the editor
        """
        raise NotImplementedError('The "run" method is not implemented.')
