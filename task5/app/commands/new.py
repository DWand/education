from app.commands.basecommand import BaseCommand
from app.editor import Document


class NewCommand(BaseCommand):
    """
    A CLI command which allows the user to create a new image
    """

    @classmethod
    def run(cls, state):
        print 'Enter parameters of the document:'
        name = cls._get_string('name')
        width = cls._get_number('width')
        height = cls._get_number('height')

        state.document = Document(name, width, height)

        print 'A new document successfully created!'
