from app.commands.basecommand import BaseCommand


class DelShapeCommand(BaseCommand):
    """
    A CLI command which allows the user to delete a shape from the image
    """

    @classmethod
    def run(cls, state):
        if state.document is None:
            print 'There are no documents opened.'
        else:
            print 'Enter an index of the item to delete or -1 to cancel.'
            index = cls._get_number('index')
            if index != -1:
                state.document.del_shape(index)
