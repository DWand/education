class State(object):
    """ A current state of the editor """

    def __init__(self, document):
        """
        :param editor.Document document: A document the editor is working on
            at the moment
        """
        self.document = document
