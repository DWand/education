import turtle
import math


def _frange(start, stop, step):
    """
    Generates a sequence of floating point numbers between start and
    stop values with the given step.

    :param float start: A value to begin generation from
    :param float stop: An indicator to stop generation
    :param float step: A difference between two sequential values
    """
    cur = start
    while cur < stop:
        yield cur
        cur += step


class Graphics(object):
    """ Draws primitives in the windows """

    def __init__(self, width, height):
        """
        Initializes a graphics object.

        :param int width: A width of the window
        :param int height: A height of the windows
        """
        self._wnd = turtle.Screen()
        self._wnd.screensize(width, height)
        self._wnd.setworldcoordinates(0, height, width, 0)

        self._turtle = turtle.Turtle()
        self._turtle.hideturtle()
        self._turtle.tracer(0)
        self._turtle.penup()

    def invalidate(self):
        """
        Shows all drawn primitives and makes it possible to close the window.
        """
        self._wnd.update()
        self._wnd.exitonclick()

    def draw_point(self, x, y, color):
        """
        Draws a point.

        :param int x: an X coordinate of the point
        :param int y: an Y coordinate of the point
        :param color: a color of the point
        :type color: tuple or str
        """
        pen = self._turtle
        pen.color(color, color)
        pen.goto(x, y)
        pen.pendown()
        pen.dot(1, color)
        pen.penup()

    def draw_line(self, x1, y1, x2, y2, color):
        """
        Draws a line.

        :param int x1: an X coordinate of the start point of the line
        :param int y1: an Y coordinate of the start point of the line
        :param int x2: an X coordinate of the end point of the line
        :param int y2: an Y coordinate of the end point of the line
        :param color: a color of the line
        :type color: tuple or str
        """
        pen = self._turtle
        pen.color(color, color)
        pen.goto(x1, y1)
        pen.pendown()
        pen.goto(x2, y2)
        pen.penup()

    def draw_rect(self, x1, y1, x2, y2, color):
        """
        Draws a rectangle.

        :param int x1: an X coordinate of the top left corner of the rectangle
        :param int y1: an Y coordinate of the top left corner of the rectangle
        :param int x2: an X coordinate of the bottom right corner
            of the rectangle
        :param int y2: an Y coordinate of the bottom right corner
            of the rectangle
        :param color: a color of the rectangle
        :type color: tuple or str
        """
        pen = self._turtle
        pen.color(color, color)
        pen.goto(x1, y1)
        pen.pendown()
        pen.begin_fill()
        pen.goto(x2, y1)
        pen.goto(x2, y2)
        pen.goto(x1, y2)
        pen.end_fill()
        pen.penup()

    def draw_ellipse(self, x1, y1, x2, y2, color):
        """
        Draws an ellipse.

        :param int x1: an X coordinate of the top left corner
            of the enclosing rectangle
        :param int y1: an Y coordinate of the top left corner
            of the enclosing rectangle
        :param int x2: an X coordinate of the bottom right corner
            of the enclosing rectangle
        :param int y2: an Y coordinate of the bottom right corner
            of the enclosing rectangle
        :param color: a color of the ellipse
        :type color: tuple or str
        """
        pen = self._turtle
        pen.color(color, color)
        cx = (x1 + x2) / 2
        cy = (y1 + y2) / 2
        xr = abs(x2 - x1) / 2
        yr = abs(y2 - y1) / 2
        x = cx + xr * math.cos(0)
        y = cy + yr * math.sin(0)
        pen.goto(x, y)
        pen.pendown()
        pen.begin_fill()
        for theta in _frange(0, math.radians(360), math.radians(1)):
            x = cx + xr * math.cos(theta)
            y = cy + yr * math.sin(theta)
            pen.goto(x, y)
        pen.end_fill()
        pen.penup()

    def draw_polygon(self, points, color):
        """
        Draws a polygon.

        :param points: a list of points the polygon consists of
        :type points: a list of (int,int) tuples
        :param color: a color of the polygon
        :type color:
        """
        pen = self._turtle
        pen.color(color, color)
        pen.goto(points[0][0], points[0][1])
        pen.pendown()
        pen.begin_fill()
        for point in points[1:]:
            pen.goto(point[0], point[1])
        pen.end_fill()
        pen.penup()
