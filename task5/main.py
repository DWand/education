#!/usr/bin/python

""" An entry point of the application """


from app.application import Application


def main():
    """
    Initializes and runs an application
    """
    app = Application()
    app.run()


if __name__ == '__main__':
    main()
