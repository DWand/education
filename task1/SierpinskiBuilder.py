
class SierpinskiBuilder(object):
    """
    An abstract Sierpinski Triangle builder
    """

    def __init__(self, base_triangle):
        """
        Initializes boundaries on the triangle

        Args:
            base_triangle (Triangle): A triangle which represents boundaries
        """
        self._base = base_triangle


    def build(self):
        """
        Builds a sierpinski triangle

        Raises:
            NotImplementedError: This method has to be defined in child classes
        """
        raise NotImplementedError('The "build" method is not implemented')


    def draw(self, turle):
        """
        Draws a sierpinski triangle

        Args:
            turle (Turtle): A turle which has to be used for drawing

        Raises:
            NotImplementedError: This method has to be defined in child classes
        """
        raise NotImplementedError('The "draw" method is not implemented')
