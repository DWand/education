from random import randint

from SierpinskiBuilder import SierpinskiBuilder

class ChaosBuilder(SierpinskiBuilder):
    """
    A chaos Sierpinski Triangle builder
    """

    def __init__(self, base_triangle, start_point, points_amount):
        """
        Initializes a chaos builder

        Args:
            base_triangle (Triangle): A triangle which represents boundaries
            start_point (Point): A starting point for the alhorithm
            points_amount (int): An amount of points to generate
        """
        self.__points = []
        self.points_amount = points_amount
        self.start_point = start_point
        super(ChaosBuilder, self).__init__(base_triangle)


    def __get_next_point(self):
        """
        Calculates next point of the triangle
        """
        prev = self.__points[-1]
        attr_index = randint(0, 2)
        attractor = self.__points[attr_index]
        return ((prev[0] + attractor[0]) / 2, (prev[1] + attractor[1]) / 2)


    def build(self):
        """
        Builds a Sierpinski Triangle in a chaotic manner
        """
        self.__points = [
            self._base.vertices[0].to_tuple(),
            self._base.vertices[1].to_tuple(),
            self._base.vertices[2].to_tuple(),
            self.start_point.to_tuple()
        ]
        for i in range(0, self.points_amount):
            next_point = self.__get_next_point()
            self.__points.append(next_point)


    def draw(self, turtle):
        """
        Draws a Sierpinski Triangle
        """
        turtle.penup()
        for point in self.__points:
            turtle.goto(point[0], point[1])
            turtle.pendown()
            turtle.dot(1)
            turtle.penup()
