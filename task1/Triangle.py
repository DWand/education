from math import sqrt
from Point import Point


class Triangle(object):
    """
    Represents 2D equilateral triangle
    """

    def __init__(self, top, side):
        """
        Initializes equilateral triangle

        Args:
            top (Point): A top point of the triangle
            side (float): A length of the triangle's side
        """
        height = Triangle.calc_height(side)
        self.side = side
        self.vertices = [
            top,
            Point(top.x + side / 2, top.y - height),
            Point(top.x - side / 2, top.y - height)
        ]


    def draw(self, turtle):
        """
        Draws the triangle

        Args:
            turtle (Turtle): A turtle to draw with
        """
        turtle.penup()
        turtle.goto(self.vertices[0].x, self.vertices[0].y)

        turtle.begin_fill()
        turtle.pendown()
        for i in range(1, 3):
            turtle.goto(self.vertices[i].x, self.vertices[i].y)
        turtle.penup()
        turtle.end_fill()


    def calc_edge_center(self, edge_index):
        """
        Calculates a center of the given edge

        Args:
            edge_index (int): An index of the target edge. Note that an index of
            the edge is the same a an index of its opposite vertex

        Returns:
            Point: A center point of the edge
        """
        begin = self.vertices[(edge_index + 1) % 3]
        end = self.vertices[(edge_index + 2) % 3]
        x_shift = (end.x - begin.x) / 2
        y_shift = (end.y - begin.y) / 2
        return Point(begin.x + x_shift, begin.y + y_shift)


    def split(self):
        """
        Splits the triangle into 3 smaller triangles

        Retruns:
            Triangle[]: A list of new triangles
        """
        new_side = self.side / 2
        center_right = self.calc_edge_center(2)
        center_left = self.calc_edge_center(1)
        triangles = [
            Triangle(self.vertices[0], new_side),
            Triangle(center_right, new_side),
            Triangle(center_left, new_side)
        ]
        return triangles


    def calc_height(side):
        """
        Calculates height of the equilateral triangle

        Args:
            side (float): A length of triangle's side

        Returns:
            float: A height of the triangle
        """
        return sqrt(3) / 2 * side

    calc_height = staticmethod(calc_height)
