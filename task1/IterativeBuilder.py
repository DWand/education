from SierpinskiBuilder import SierpinskiBuilder

class IterativeBuilder(SierpinskiBuilder):
    """
    An iterative Sierpinski Triangle builder
    """

    def __init__(self, base_triangle, iterations):
        """
        Initializes an iterative builder

        Args:
            base_triangle (Triangle): A triangle which represents boundaries
            iterations (int): An amount of iterations to perform
        """
        self._iterations = iterations
        self.__triangles = []
        super(IterativeBuilder, self).__init__(base_triangle)


    def __perform_iteration(triangles):
        """
        Performs an iteration of the alhorithm. It breaks each given triangle
        into 3 smaller ones.

        Args:
            triangles (Triangle[]): A list of triangles from the previous
            iteration

        Returns:
            Triangle[]: A new list of triangles
        """
        new_triangles = []
        for triangle in triangles:
            new_triangles.extend(triangle.split())
        return new_triangles

    __perform_iteration = staticmethod(__perform_iteration)


    def build(self):
        """
        Builds a Sierpinski Triangle in an iterative manner
        """
        triangles = [self._base]
        for i in range(0, self._iterations):
            triangles = IterativeBuilder.__perform_iteration(triangles)
        self.__triangles = triangles


    def draw(self, turtle):
        """
        Draws a Sierpinski Triangle
        """
        for triangle in self.__triangles:
            triangle.draw(turtle)
