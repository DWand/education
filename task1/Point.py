from math import sqrt

class Point(object):
    """
    Represents 2D point
    """

    def __init__(self, x, y):
        """
        Performs initialization of the point

        Args:
            x (float): x coordinate of the point
            y (float): y coordinate of the point
        """
        self.x = x
        self.y = y


    def distance_to(self, other):
        """
        Calculates distance to another point

        Args:
            other (Point): A point to calculate distance to

        Returns:
            float: A distance between points
        """
        return sqrt((self.x - other.x) ** 2 + (self.y - other.y) ** 2)


    def to_tuple(self):
        """
        Represents the point as tuple
        """
        return (self.x, self.y)
