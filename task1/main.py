#!/usr/bin/python

import turtle

from Triangle import Triangle
from Point import Point
from IterativeBuilder import IterativeBuilder
from ChaosBuilder import ChaosBuilder


LINE_COLOR = 'black'
FILL_COLOR = 'black'


def get_builder(triangle):
    """
    Constructs a builder

    Args:
        triangle (Triangle): A base triangle
    """
    print 'Select an algo:'
    print '[1]: Iterative'
    print '[2]: Chaos'
    algo = int(raw_input())
    if algo == 1:
        return init_iterative_builder(triangle)
    else:
        return init_chaos_builder(triangle)


def init_iterative_builder(triangle):
    """
    Constructs an iterative builder

    Args:
        triangle (Triangle): A base triangle
    """
    iters = int(raw_input('How many iterations do you want? '))
    return IterativeBuilder(triangle, iters)


def init_chaos_builder(triangle):
    """
    Constructs a chaos builder

    Args:
        triangle (Triangle): A base triangle
    """
    start_x = float(raw_input('Input starting point (x coordinate): '))
    start_y = float(raw_input('Input starting point (y coordinate): '))
    points = int(raw_input('How many points do you want to generate? '))
    return ChaosBuilder(triangle, Point(start_x, start_y), points)

def main():
    base_triangle = Triangle(Point(0, 250), 500)

    builder = get_builder(base_triangle)

    wnd = turtle.Screen()
    wnd.title('Triangle')
    wnd.setup(800, 600)

    ttl = turtle.Turtle()
    ttl.hideturtle()
    ttl.color(LINE_COLOR)
    ttl.fillcolor(FILL_COLOR)
    ttl.speed(0)

    builder.build()
    builder.draw(ttl)

    print 'Done'

    turtle.mainloop()


if __name__ == "__main__":
    main()
