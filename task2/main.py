#!/usr/bin/python

"""
Simple charts drawer program
"""

import turtle

from chart import SectorsChart, RaysChart



def _get_string():
    """
    Retrieves a string to work with

    Return:
        str: A string to build a chart for
    """
    return raw_input('What string do you want to use? ')


def _get_mode():
    """
    Retrieves a mode to work with

    Return:
        str: A type of diagram to draw (sectors for something else)
    """
    return raw_input('What mode do you want to use? (sectors or rays) ')


def draw(string, mode, turtle):
    """ Draws a chart for the requested string """
    if mode == 'sectors':
        chart = SectorsChart(string, 400)
    else:
        chart = RaysChart(string, 400)
    chart.draw(turtle)


def main():
    """Main entry point of the script"""

    string = _get_string()
    mode = _get_mode()

    turtle.colormode(255)

    wnd = turtle.Screen()
    wnd.title('Chart')
    wnd.setup(800, 600)

    ttl = turtle.Turtle()
    # ttl.hideturtle()
    ttl.color(255, 255, 255)
    ttl.speed(0)

    draw(string, mode, ttl)

    print('Done')
    turtle.mainloop()


if __name__ == '__main__':
    main()
