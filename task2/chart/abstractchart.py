from random import randint
from re import split


class AbstractChart(object):
    """ Abstract chart """

    LABEL_SHIFT = 20
    LABEL_COLOR = 'black'

    def __init__(self, string, size):
        """
        Initializes a chart

        Args:
            string (str): A string to build a chart for
            size (int): A size of the chart
        """
        self.string = string
        self.size = size
        self._split_string()
        self._calc_weights()
        self._generate_colors()

    def _split_string(self):
        """ Splits string into separate words """
        string = self.string.lower().strip()
        if len(string) > 0:
            self._words = split('\s+', string)
        else:
            self._words = []

    def _calc_weights(self):
        """ Calculates weight of each word in the string """
        self._weights = {}
        for word in self._words:
            if word not in self._weights:
                self._weights[word] = 1
            else:
                self._weights[word] += 1

    def _generate_colors(self):
        """ Generates a random color for each word in the string """
        self._colors = {}
        for word in self._words:
            if word not in self._colors:
                r = randint(100, 255)
                g = randint(100, 255)
                b = randint(100, 255)
                self._colors[word] = (r, g, b)

    def draw(self, turtle):
        """
        Draws a chart

        Args:
            turtle (Turtle): A turtle to use for drawing
        """
        raise NotImplementedError('The "draw" method has to be \
                                   implemented by its subclasses')
