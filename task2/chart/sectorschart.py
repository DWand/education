from math import pi, cos, sin

from abstractchart import AbstractChart


def _frange(start, stop, step):
    """
    Generates a sequence of floating point numbers between start and
    stop values with the given step

    Args:
        start (float): A value to begin generation from
        stop (float): An indicator to stop generation
        step (float): A difference between two sequentional values
    """
    cur = start
    while cur < stop:
        yield cur
        cur += step


def _get_point_coords(center_x, center_y, radius, angle):
    """
    Calculates a position of a point based on radius and angle

    Args:
        center_x (float): X coordinate of the center
        center_y (float): Y coordinate of the center
        radius (float): A distance between center and needed point
        angle (float): An angle in radians from OX axis
    """
    x_coord = center_x + radius * cos(angle)
    y_coord = center_y + radius * sin(angle)
    return (x_coord, y_coord)


class SectorsChart(AbstractChart):
    """ Sectors chart """

    def __init__(self, string, size):
        """
        Initializes a chart

        Args:
            string (str): A string to build a chart for
            size (int): A size of the chart
        """
        super(SectorsChart, self).__init__(string, size)
        self._total_words = len(self._words)

    def draw(self, turtle):
        """
        Draws a chart

        Args:
            turtle (Turtle): A turtle to use for drawing
        """
        if len(self._weights) == 0:
            return

        total_angle = 0
        for word in self._weights.keys():
            weight = self._weights[word]
            color = self._colors[word]
            angle = weight / float(self._total_words) * 2 * pi

            self._fill_circle_part(turtle, color,
                                   total_angle, total_angle + angle)

            total_angle += angle

        self._draw_legend(turtle)


    def _fill_circle_part(self, turtle, color, start_angle, end_angle):
        """
        Fills a part of the chart circle between the specified angles

        Args:
            turtle (Turtle): A turtle to use for drawing
            color (Tuple): A color to use for filling section
            start_angle (float): A starting angle of the circle section
            end_angle (float): An end angle of he circle section
        """
        radius = (self.size - self.LABEL_SHIFT * 2) / 2
        start_x = 0
        start_y = 0
        step_angle = 1 * pi / 180

        turtle.penup()
        turtle.setpos(start_x, start_y)
        turtle.fillcolor(color)

        turtle.begin_fill()
        turtle.pendown()

        for theta in _frange(start_angle, end_angle, step_angle):
            point = _get_point_coords(start_x, start_y, radius, theta)
            turtle.setpos(point[0], point[1])

        point = _get_point_coords(start_x, start_y, radius, end_angle)
        turtle.setpos(point[0], point[1])
        turtle.setpos(start_x, start_y)

        turtle.end_fill()


    def _draw_legend(self, turtle):
        """
        Draws legend of the chart

        Args:
            turtle (Turtle): A turtle to use for drawing
        """
        initial_color = turtle.color()
        turtle.penup()
        turtle.goto(self.size / 2.0, self.size / 2.0)

        for word, color in self._colors.iteritems():
            turtle.color(color)
            turtle.pendown()
            turtle.dot(10)
            turtle.penup()

            turtle.forward(self.LABEL_SHIFT)

            turtle.right(90)
            turtle.forward(5)
            turtle.left(90)
            turtle.color(self.LABEL_COLOR)
            turtle.write(word, align='left')

            turtle.backward(self.LABEL_SHIFT)
            turtle.right(90)
            turtle.forward(self.LABEL_SHIFT)
            turtle.left(90)

        turtle.color(initial_color[0], initial_color[1])
