from abstractchart import AbstractChart


class RaysChart(AbstractChart):
    """ Rays chart """

    def draw(self, turtle):
        """
        Draws a chart

        Args:
            turtle (Turtle): A turtle to use for drawing
        """
        if len(self._weights) == 0:
            return

        max_steps = max(self._weights.values())
        rays_count = len(self._weights)
        angle = 360 / rays_count
        step_size = (self.size - self.LABEL_SHIFT * 2) / 2.0 / float(max_steps)

        for word in self._weights.keys():
            weight = self._weights[word]
            color = self._colors[word]

            turtle.color(color)
            turtle.goto(0, 0)
            turtle.pendown()
            for step in range(0, weight):
                turtle.forward(step_size)
                turtle.dot(10)
            turtle.penup()

            turtle.forward(self.LABEL_SHIFT)
            turtle.pendown()
            turtle.color(self.LABEL_COLOR)
            turtle.write(word, align='center')
            turtle.penup()

            turtle.left(angle)
